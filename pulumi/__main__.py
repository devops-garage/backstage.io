import pulumi
import pulumi_yandex as yandex

config = pulumi.Config()

foo_vpc_network = yandex.VpcNetwork("fooVpcNetwork")

foo_vpc_subnet = yandex.VpcSubnet("fooVpcSubnet",
    network_id=foo_vpc_network.id,
    v4_cidr_blocks=["10.2.0.0/16"],
    zone="ru-central1-a")

sshkey = config.require('sshkey')

with open('cloud-config') as f:
    cloud_config = f.read()

default = yandex.ComputeInstance("default",
    boot_disk=yandex.ComputeInstanceBootDiskArgs(
        initialize_params=yandex.ComputeInstanceBootDiskInitializeParamsArgs(
            image_id="fd8vgqmrilk8dchj1ccf",
            size=20,
        ),
    ),
    metadata={
        "foo": "bar",
        "ssh-keys": f"ubuntu:{sshkey}",
        "user-data": cloud_config,
    },
    network_interfaces=[yandex.ComputeInstanceNetworkInterfaceArgs(
        subnet_id=foo_vpc_subnet.id,
        nat=True, 
    )],
    platform_id="standard-v1",
    resources=yandex.ComputeInstanceResourcesArgs(
        cores=2,
        memory=4,
    ),
    zone="ru-central1-a")

pulumi.export('public_ip', default.network_interfaces[0].nat_ip_address)
pulumi.export('internal_ip', default.network_interfaces[0].ip_address)
